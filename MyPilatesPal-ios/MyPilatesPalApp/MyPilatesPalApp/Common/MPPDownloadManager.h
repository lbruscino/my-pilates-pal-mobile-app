//
//  MPPDownloadManager.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 3/9/14.
//  Copyright (c) 2014 My Pilates Pal. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MPPDownloadManagerDelegate
- (void)error:(NSError *)error downloading:(NSString *)url current:(NSInteger)current of:(NSInteger)number;
- (void)finishedDownloadingURL:(NSURL *)url withAssetPath:(NSString *)assetPath current:(NSInteger)current of:(NSInteger)number;
- (void)finishedDownloading:(NSInteger)successful outOf:(NSInteger)total;
- (void)errorDownloading;
@end

@interface MPPDownloadManager : NSObject
- (id)initWithDelegate:(id<MPPDownloadManagerDelegate>)downloadDelegate withList:(NSArray *)downloadList;
- (void)start;
@end
