//
//  MPPVideo.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 11/22/14.
//  Copyright (c) 2014 My Pilates Pal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPPVideo : NSObject
+ (instancetype)videoFromDictionary:(NSDictionary *)dictionary;
@property NSNumber *order;
@property NSString *name;
@property NSNumber *duration;
@property NSString *extension;
@property NSString *s3_path;
@property NSString *instructor;
@property NSString *path;
@end
