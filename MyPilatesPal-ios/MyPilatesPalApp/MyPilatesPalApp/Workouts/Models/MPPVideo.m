//
//  MPPVideo.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 11/22/14.
//  Copyright (c) 2014 My Pilates Pal. All rights reserved.
//

#import "MPPVideo.h"

@implementation MPPVideo
+ (instancetype)videoFromDictionary:(NSDictionary *)dictionary {
    MPPVideo *video = [[MPPVideo alloc] init];
    video.order = [dictionary valueForKey:@"order"];
    video.name = [dictionary valueForKey:@"name"];
    video.duration = [dictionary valueForKey:@"duration"];
    video.extension = [dictionary valueForKey:@"extension"];
    video.instructor = [dictionary valueForKey:@"instructor"];
    video.path = [dictionary valueForKey:@"path"];
    
    return video;
}
@end
