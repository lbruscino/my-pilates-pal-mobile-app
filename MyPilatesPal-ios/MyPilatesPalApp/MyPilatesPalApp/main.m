//
//  main.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MPPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MPPAppDelegate class]));
    }
}
