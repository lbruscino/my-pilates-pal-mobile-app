//
//  MPPPose.h
//  MyPilatesPal
//
//  Created by PAWAN POUDEL on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

@interface MPPPose : NSObject

+ (instancetype)poseFromDictionary:(NSDictionary *)dictionary;
@property (nonatomic, strong) NSNumber *uid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *duration;
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic, strong) NSString *repetition;
@property (nonatomic, strong) NSString *soundTrackPath;
@property (nonatomic, strong) UIImage *locallyCachedImage;
@property (nonatomic, strong) NSData *locallyCachedSound;
@property (nonatomic, strong) NSURL *locallySoundURL;

@end
