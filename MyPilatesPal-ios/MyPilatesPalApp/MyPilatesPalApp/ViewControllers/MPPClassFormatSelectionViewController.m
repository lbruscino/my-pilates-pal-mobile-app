//
//  MPPClassFormatSelectionViewController.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 3/21/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import "MPPClassFormatSelectionViewController.h"
#import "MPPWorkoutManager.h"
#import "MPPClassVideoContainerViewController.h"
#import "MPPClassesViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <ECSlidingViewController/ECSlidingViewController.h>

@interface MPPClassFormatSelectionViewController ()
@property MPPWorkoutType workout;
@property MPPWorkoutDurationType duration;
@property (strong, nonatomic) MPPWorkoutManager *workoutManager;
@property (assign, nonatomic) id<MPPClassFormatSelectionDelegate> classFormatSelectionDelegate;
@property (strong, nonatomic) MPPAnalyticsTracker *analyticsTracker;
@end

@implementation MPPClassFormatSelectionViewController

- (id)initWithWorkoutType:(MPPWorkoutType)workoutType withDuration:(MPPWorkoutDurationType)workoutDuration withClassFormatDelegate:(id<MPPClassFormatSelectionDelegate>)selectionDelegate {
    self = [super init];
    
    self.workout = workoutType;
    self.duration = workoutDuration;
    self.workoutManager = [[MPPWorkoutManager alloc] init];
    self.classFormatSelectionDelegate = selectionDelegate;
    self.analyticsTracker = [[MPPAnalyticsTracker alloc] init];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated]; 
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)playVideo:(id)sender {
    [self.analyticsTracker trackUserInteractionWithAction:kActionButton label:@"Video Format" value:nil];
    [self dismissViewControllerAnimated:YES completion:^{
        [self.classFormatSelectionDelegate choseClassFormat:MPPClassFormatVideo withWorkout:self.workout withDuration:self.duration];
    }];
}

- (IBAction)playFlashCards:(id)sender {
    [self.analyticsTracker trackUserInteractionWithAction:kActionButton label:@"Flash Card Format" value:nil];    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.classFormatSelectionDelegate choseClassFormat:MPPClassFormatFlashcard withWorkout:self.workout withDuration:self.duration];
    }];
}


@end
