//
//  MPPAccountView.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 1/3/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPPAccountView : UIView
@property (weak, nonatomic) UINavigationController *navigationController;
- (void)tapped;
@end
