//
//  MPPCategory.h
//  MyPilatesPal
//
//  Created by PAWAN POUDEL on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

@interface MPPCategory : NSObject

@property (nonatomic) NSInteger uid;
@property (nonatomic, strong) NSString *name;

@end
