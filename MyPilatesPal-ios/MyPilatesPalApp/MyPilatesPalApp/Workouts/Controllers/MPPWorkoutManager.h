//
//  MPPWorkoutManager.h
//  MyPilatesPal
//
//  Created by PAWAN POUDEL on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import "MPPWorkoutManagerDelegate.h"

extern NSString *MPPWorkoutManagerError;

enum {
    MPPWorkoutManagerErrorFetchCategoriesCode,
    MPPWorkoutManagerErrorFetchWorkoutCode
};

@interface MPPWorkoutManager : NSObject

- (void)fetchWorkoutWithCategory:(NSInteger)categoryID
                    withDuration:(NSInteger)duration
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
- (void)fetchVideoWorkout:(NSInteger)workoutID
                  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
@end
