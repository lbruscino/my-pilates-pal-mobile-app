//
//  MPPAnalyticsTracker+AdBanner.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 12/8/15.
//  Copyright © 2015 My Pilates Pal. All rights reserved.
//

#import "MPPAnalyticsTracker.h"
@import iAd;

@interface MPPAnalyticsTracker (AdBanner) <ADBannerViewDelegate>

@end
