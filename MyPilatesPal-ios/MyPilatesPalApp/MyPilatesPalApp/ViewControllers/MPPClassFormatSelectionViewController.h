//
//  MPPClassFormatSelectionViewController.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 3/21/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

typedef NS_ENUM(NSInteger, MPPClassFormat) {
    MPPClassFormatVideo,
    MPPClassFormatFlashcard
};

@protocol MPPClassFormatSelectionDelegate <NSObject>
- (void)choseClassFormat:(MPPClassFormat)classFormat withWorkout:(MPPWorkoutType)workoutType withDuration:(MPPWorkoutDurationType)duration;
@end

@interface MPPClassFormatSelectionViewController : GAITrackedViewController

- (id)initWithWorkoutType:(MPPWorkoutType)workoutType withDuration:(MPPWorkoutDurationType)workoutDuration withClassFormatDelegate:(id<MPPClassFormatSelectionDelegate>)selectionDelegate;
@end
