//
//  MPPClassVideoViewController.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 11/15/14.
//  Copyright (c) 2014 My Pilates Pal. All rights reserved.
//

#import "MPPClassVideoViewController.h"
#import "MPPVideoWorkout.h"
#import <VKVideoPlayer/VKVideoPlayer.h>
#import "MPPPose.h"

@interface MPPClassVideoViewController ()<VKVideoPlayerDelegate>
@property (weak, nonatomic) IBOutlet UIView *videoPlaceHolder;
@property (strong, nonatomic) VKVideoPlayer* videoPlayer;
@property (assign, nonatomic) NSInteger currentVideo;
@property (strong, nonatomic) MPPAnalyticsTracker *analyticsTracker;
@end

@implementation MPPClassVideoViewController

- (id)init {
    self = [super init];
    if(self) {

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.videoPlayer = [[VKVideoPlayer alloc] init];
    self.videoPlayer.delegate = self;
    self.videoPlayer.view.frame = self.videoPlaceHolder.bounds;
    self.videoPlayer.view.playerControlsAutoHideTime = @10;
    [self.videoPlaceHolder addSubview:self.videoPlayer.view];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self playStreamFromBeginning];
}


- (void)playStreamFromBeginning {
    [self playStreamAtIndex:0];
}

- (void)playStreamAtIndex:(NSInteger)streamIndex {
    self.currentVideo = streamIndex;
    MPPVideo *videoWorkout = [self.videoWorkout.videos objectAtIndex:streamIndex];
    NSURL *trackURL = [NSURL URLWithString:videoWorkout.path];
    VKVideoPlayerTrack *track = [[VKVideoPlayerTrack alloc] initWithStreamURL:trackURL];
    track.hasNext = YES;
    [self.videoPlayer loadVideoWithTrack:track];
}

- (void)pauseCurrent {
    [self.videoPlayer pauseContent];
}

- (void)resumeCurrent {
    [self.videoPlayer playContent];
}

#pragma mark VKVideoPlayerDelegate
- (void)videoPlayer:(VKVideoPlayer*)videoPlayer didPlayToEnd:(id<VKVideoPlayerTrackProtocol>)track {
    MPPVideo *video = [self.videoWorkout.videos objectAtIndex:self.currentVideo];
    NSString *userAction = [NSString stringWithFormat:@"Video Finished %@", video.name];
    [self.analyticsTracker trackUserInteractionWithAction:userAction label:nil value:nil];
    self.currentVideo++;
    if(self.currentVideo <  self.videoWorkout.videos.count) {
        [self playStreamAtIndex:self.currentVideo];
    } else {
        [self.videoPlayDelegate finishedWorkout];
    }
}

- (void)handleErrorCode:(VKVideoPlayerErrorCode)errorCode track:(id<VKVideoPlayerTrackProtocol>)track customMessage:(NSString *)customMessage {
    MPPVideo *video = [self.videoWorkout.videos objectAtIndex:self.currentVideo];
    NSString *errorAction = [NSString stringWithFormat:@"Error Playing Video %@", video.name];
    [self.analyticsTracker trackErrorAction:errorAction label:[@(errorCode) stringValue] value:nil];
#if DEBUG
    NSLog(@"Handle error: %@", [@(errorCode) stringValue]);
#endif
}

@end
