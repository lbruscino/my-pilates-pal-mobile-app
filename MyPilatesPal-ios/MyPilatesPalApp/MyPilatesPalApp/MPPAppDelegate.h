//
//  MPPAppDelegate.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MPPWorkoutTypeSetting) {
    MPPWorkoutTypeSettingVideo,
    MPPWorkoutTypeSettingFlashCard
};

@interface MPPAppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (assign, nonatomic) MPPWorkoutTypeSetting workoutTypeSetting;

+ (MPPAppDelegate *)appDelegate;
@end
