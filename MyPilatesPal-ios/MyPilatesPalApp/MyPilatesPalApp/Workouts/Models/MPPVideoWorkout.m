//
//  MPPVideoWorkout.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 8/5/14.
//  Copyright (c) 2014 My Pilates Pal. All rights reserved.
//

#import "MPPVideoWorkout.h"
#import "MPPVideo.h"
@implementation MPPVideoWorkout

+ (instancetype)videoWorkoutFromDictionary:(NSDictionary *)dictionary {
    MPPVideoWorkout *videoWorkout = [[MPPVideoWorkout alloc] init];
    
    NSMutableArray *videoArray = [NSMutableArray new];
    NSDictionary *videoDictionaries = [dictionary valueForKey:@"videos"];
    for (NSDictionary *videoDictionary in videoDictionaries) {
        MPPVideo *video = [MPPVideo videoFromDictionary:videoDictionary];
        [videoArray addObject:video];
    }
    
    videoWorkout.videos = videoArray;    
    return videoWorkout;
}
@end
