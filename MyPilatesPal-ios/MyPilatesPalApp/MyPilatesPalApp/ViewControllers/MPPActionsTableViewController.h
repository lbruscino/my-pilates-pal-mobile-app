//
//  MPPActionsTableViewController.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 1/3/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MPPActionsDelegate <NSObject>
- (void)action:(NSString *)action;
@end

@interface MPPActionsTableViewController : UITableViewController
- (instancetype)initWithActions:(NSArray *)actions withActions:(id<MPPActionsDelegate>)actionsDelegate;
@property (nonatomic) UIColor *tableColor;
@end
