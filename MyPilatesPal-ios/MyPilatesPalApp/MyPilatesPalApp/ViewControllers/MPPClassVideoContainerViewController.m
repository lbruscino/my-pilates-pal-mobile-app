//
//  MPPClassVideoContainerViewController.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 4/4/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import "MPPClassVideoContainerViewController.h"
#import "MPPVideoPlayListTableViewController.h"
#import "MPPClassVideoViewController.h"
#import "MPPVideoPlayListTableViewController.h"
#import "MPPClassFinishedViewController.h"
#import "UIViewController+AdBanner.h"
@import iAd;

@interface MPPClassVideoContainerViewController ()<MPPVideoPlayDelegate>
@property (nonatomic, strong) MPPClassVideoViewController *videoController;
@property (nonatomic, strong) MPPVideoPlayListTableViewController *playListController;
@property (nonatomic, strong) MPPVideoWorkout *videoWorkout;
@property (nonatomic, strong) MPPWorkout *workout;
@property (nonatomic, strong) MPPAnalyticsTracker *analyticsTracker;
@end

@implementation MPPClassVideoContainerViewController

- (instancetype)initWithWorkout:(MPPWorkout *)workout andVideoWorkout:(MPPVideoWorkout *)videoWorkout {
    self = [super init];
    self.videoWorkout = videoWorkout;
    self.workout = workout;
    
    self.videoController = [[MPPClassVideoViewController alloc] init];
    self.videoController.videoWorkout = self.videoWorkout;
    self.videoController.videoPlayDelegate = self;
    self.topViewController = self.videoController;
    self.anchorLeftRevealAmount = 340;
    self.analyticsTracker = [[MPPAnalyticsTracker alloc] init];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Playlist" style:UIBarButtonItemStylePlain target:self action:@selector(showPlaylist:)]];
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.playListController = [[MPPVideoPlayListTableViewController alloc] initWithVideoWorkout:self.videoWorkout];
    [self.playListController setVideoPlayDelegate:self];
    self.underRightViewController = self.playListController;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self addBannerWithDelegate:[[MPPAnalyticsTracker alloc] init]];
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    BOOL shouldExecuteAction = [self allowActionToRun]; // your app implements this method
    if (!willLeave && shouldExecuteAction)
    {
        [self pauseWorkout];
    }
    return shouldExecuteAction;
}

- (BOOL)allowActionToRun {
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner {
    [self resumeWorkout];
}

- (void)pauseWorkout {
    [self.videoController pauseCurrent];
}

- (void)resumeWorkout {
    [self.videoController resumeCurrent];
}

- (void)playTrack:(NSInteger)trackNumber {
    [self closePlaylist:nil];
    [self.videoController playStreamAtIndex:trackNumber];
}

- (void)finishedWorkout {
    MPPClassFinishedViewController *classFinished = [[MPPClassFinishedViewController alloc] initWithFinishedClassName:self.workout.title];
    UINavigationController * navigationController = self.navigationController;
    [navigationController popToRootViewControllerAnimated:NO];
    [navigationController pushViewController:classFinished animated:YES];
}

- (void)showPlaylist:(id)sender {
    if (self.currentTopViewPosition == ECSlidingViewControllerTopViewPositionAnchoredLeft) {
        [self closePlaylist:sender];
    } else {
        [self openPlaylist:sender];
    }
}

- (void)openPlaylist:(id)sender {
    [self.analyticsTracker trackUserInteractionWithAction:kActionButton label:@"Show Playlist" value:nil];
    [self anchorTopViewToLeftAnimated:YES];
}

- (void)closePlaylist:(id)sender {
    [self.analyticsTracker trackUserInteractionWithAction:kActionButton label:@"Close Playlist" value:nil];
    [self resetTopViewAnimated:YES];
}
@end
