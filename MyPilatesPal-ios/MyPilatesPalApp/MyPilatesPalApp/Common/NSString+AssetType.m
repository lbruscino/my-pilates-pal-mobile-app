//
//  NSString+AssetType.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 3/9/14.
//  Copyright (c) 2014 My Pilates Pal. All rights reserved.
//

#import "NSString+AssetType.h"

@implementation NSString (AssetType)

- (NSAssetType)assetType {
    if([self rangeOfString:@".m4a"].location != NSNotFound) {
        return NSAssetTypeAudio;
    } else {
        return NSAssetTypeImage;
    }
}
@end
