//
//  MPPAppDelegate.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import "MPPAppDelegate.h"
#import "MPPMainViewController.h"
#import "GAI.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <iRate/iRate.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface MPPAppDelegate ()<iRateDelegate>

@end

@implementation MPPAppDelegate

+ (MPPAppDelegate *)appDelegate {
    return (MPPAppDelegate *)[UIApplication sharedApplication].delegate;
}

- (void)setWorkoutTypeSetting:(MPPWorkoutTypeSetting)workoutTypeSetting {
    [[NSUserDefaults standardUserDefaults] setInteger:workoutTypeSetting forKey:@"WorkoutTypeSetting"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (MPPWorkoutTypeSetting)workoutTypeSetting {
    id setting = [[NSUserDefaults standardUserDefaults] objectForKey:@"WorkoutTypeSetting"];
    if(setting) {
        return [[NSUserDefaults standardUserDefaults] integerForKey:@"WorkoutTypeSetting"];
    } else {
        [[NSUserDefaults standardUserDefaults] setInteger:MPPWorkoutTypeSettingFlashCard forKey:@"WorkoutTypeSetting"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return MPPWorkoutTypeSettingFlashCard;
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self configureAnalytics];
    [self configureParseForApplication:application];
    [self configureiRate];

#if RELEASE
    [Crashlytics startWithAPIKey:@"cb98215f2b9a5310c3e29e321ba1c41031f1483a"];
#endif
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[MPPMainViewController alloc] init]];
    [navigationController setNavigationBarHidden:YES];
    [[navigationController navigationBar] setTintColor:[UIColor blackColor]];
    [[navigationController navigationBar] setBarTintColor:UIColorFromRGB(0xE0EED7)];
    
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken {
//    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
//    [currentInstallation setDeviceTokenFromData:newDeviceToken];
//    [currentInstallation saveInBackground];
    
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackServiceCallWithAction:kApplicationPushRegistration label:nil value:nil];
}

- (void)configureAnalytics {
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
#if DEBUG
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];
#else
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];
#endif
    
#if DEBUG
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-40438437-4"];
#else
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-40438437-3"];
#endif
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:kCategoryGeneralDiagnostics action:kActionStartup label:nil value:nil] build]];
}

#pragma mark Parse configuration
- (void)configureParseForApplication:(UIApplication *)application {
//    [application registerForRemoteNotificationTypes:
//     UIUserNotificationTypeBadge |
//     UIUserNotificationTypeAlert |
//     UIUserNotificationTypeSound];
//    
    
}

#pragma mark iRateDelegate + iRate Configuration
- (void)configureiRate {
    [iRate sharedInstance].delegate = self;
    [iRate sharedInstance].applicationBundleID = @"com.mypilatespal.MPPApp";
    [iRate sharedInstance].daysUntilPrompt = 1;
	[iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    [iRate sharedInstance].eventsUntilPrompt = 3;
#if DEBUG
    [iRate sharedInstance].verboseLogging = YES;
#endif
}
- (void)iRateDidPromptForRating {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackServiceCallWithAction:kServiceActioniRate label:@"prompt_user" value:nil];
}

- (void)iRateUserDidAttemptToRateApp {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionButton label:@"rate_attempt" value:nil];
}

- (void)iRateUserDidDeclineToRateApp {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionButton label:@"rate_decline" value:nil];
}

- (void)iRateUserDidRequestReminderToRateApp {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionButton label:@"rate_reminder" value:nil];
    [iRate sharedInstance].eventCount = 0;
}

@end
