//
//  MPPClassQuestionsViewController.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 11/12/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import "MPPClassQuestionsViewController.h"
#import "MBProgressHUD.h"
#import "MPPWorkoutManager.h"
#import "MPPClassesViewController.h"
#import "MPPAppDelegate.h"
#import "MPPClassVideoViewController.h"
#import "MPPClassFormatSelectionViewController.h"

@interface MPPClassQuestionsViewController () <MPPWorkoutManagerDelegate, MPPClassFormatSelectionDelegate>
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *tenTwentyLabel;
@property (weak, nonatomic) IBOutlet UIButton *tenTwentyButton;
@property (weak, nonatomic) IBOutlet UILabel *thirtyFourtyLabel;
@property (weak, nonatomic) IBOutlet UIButton *thirtyFourtyButton;
@property (weak, nonatomic) IBOutlet UILabel *fiftySixtyLabel;
@property (weak, nonatomic) IBOutlet UIButton *fiftySixtyButton;

- (IBAction)tenTwentyGo:(id)sender;
- (IBAction)thirtyFourtyGo:(id)sender;
- (IBAction)fiftySixtyGo:(id)sender;


@property (assign, nonatomic) MPPWorkoutType workout;
@property (strong, nonatomic) MPPWorkoutManager *workoutManager;
@end

@implementation MPPClassQuestionsViewController

- (id)initWithWorkoutType:(MPPWorkoutType)workoutType {
    self = [super init];
    if(self) {
        self.workout = workoutType;
        self.workoutManager = [[MPPWorkoutManager alloc] init];
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self setScreenName:@"Class Duration"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (IBAction)tenTwentyGo:(id)sender {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionButton
                                       label:[NSString stringWithFormat:@"Duration %i", 20]
                                       value:nil];
    
    id formatSelection = [[MPPClassFormatSelectionViewController alloc] initWithWorkoutType:self.workout withDuration:MPPWorkoutDurationTypeTenTwenty withClassFormatDelegate:self];
    [self.navigationController presentViewController:formatSelection
                                            animated:YES
                                          completion:^{
                                              
    }];
}

- (IBAction)thirtyFourtyGo:(id)sender {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionButton
                                       label:[NSString stringWithFormat:@"Duration %i", 40]
                                       value:nil];
    id formatSelection = [[MPPClassFormatSelectionViewController alloc] initWithWorkoutType:self.workout withDuration:MPPWorkoutDurationTypeThirtyFourty withClassFormatDelegate:self];
    [self.navigationController presentViewController:formatSelection
                                            animated:YES
                                          completion:^{
                                              
                                          }];
}

- (IBAction)fiftySixtyGo:(id)sender {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionButton
                                       label:[NSString stringWithFormat:@"Duration %i", 60]
                                       value:nil];

    id formatSelection = [[MPPClassFormatSelectionViewController alloc] initWithWorkoutType:self.workout withDuration:MPPWorkoutDurationTypeFiftySixty
                                                                    withClassFormatDelegate:self];
    [self.navigationController presentViewController:formatSelection
                                            animated:YES
                                          completion:^{
                                              
                                          }];
}

#pragma mark MPPClassFormatSelectionDelegate delegate methods
- (void)choseClassFormat:(MPPClassFormat)classFormat withWorkout:(MPPWorkoutType)workoutType withDuration:(MPPWorkoutDurationType)duration {
    if(classFormat == MPPClassFormatFlashcard) {
        [self startFlashCardWorkout:workoutType withDuration:duration];
    } else {
        [self startVideoWorkout:workoutType withDuration:duration];
    }
}

- (void)startFlashCardWorkout:(MPPWorkoutType)workoutType withDuration:(MPPWorkoutDurationType)duration {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.workoutManager fetchWorkoutWithCategory:workoutType
                                     withDuration:duration
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                              MPPWorkout *workout = [MPPWorkout workoutFromDictionary:responseObject];
                                              [MBProgressHUD hideHUDForView:self.view animated:YES];
                                              MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
                                              [tracker trackServiceCallWithAction:kServiceActionFetchWorkout
                                                                            label:[NSString stringWithFormat:@"%li", [workout.uid longValue]]
                                                                            value:nil];
                                              UIViewController *controller = [[MPPClassesViewController alloc] initWithWorkout:workout];
                                              [self.navigationController pushViewController:controller animated:YES];
                                          }
                                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                              MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
                                              [tracker trackServiceCallWithAction:kServiceActionFetchWorkoutFailed label:@"Reply" value:nil];
                                              [MBProgressHUD hideHUDForView:self.view animated:YES];
                                              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to load workout" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                              [alertView show];
                                          }];
}

- (void)startVideoWorkout:(MPPWorkoutType)workoutType withDuration:(MPPWorkoutDurationType)duration {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.workoutManager fetchWorkoutWithCategory:self.workout
                                     withDuration:3
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                              MPPWorkout *workout = [MPPWorkout workoutFromDictionary:responseObject];
                                              [self.workoutManager fetchVideoWorkout:[workout.uid integerValue] success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                  MPPVideoWorkout *videoWorkout = [MPPVideoWorkout videoWorkoutFromDictionary:responseObject];
                                                  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                                  MPPClassVideoContainerViewController *controller = [[MPPClassVideoContainerViewController alloc] initWithWorkout:workout andVideoWorkout:videoWorkout];
                                                  [self.navigationController pushViewController:controller animated:YES];
                                              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                  MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
                                                  [tracker trackServiceCallWithAction:kServiceActionFetchWorkoutVideoFailed label:@"Reply" value:nil];
                                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to get video workout" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                  [alertView show];
                                                  
                                              }];
                                          }
                                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                              MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
                                              [tracker trackServiceCallWithAction:kServiceActionFetchWorkoutFailed label:@"Reply" value:nil];
                                              [MBProgressHUD hideHUDForView:self.view animated:YES];
                                              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to load video workout" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                              [alertView show];
                                          }];
}

@end
