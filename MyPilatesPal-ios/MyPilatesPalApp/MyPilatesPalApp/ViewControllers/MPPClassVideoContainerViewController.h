//
//  MPPClassVideoContainerViewController.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 4/4/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ECSlidingViewController/ECSlidingViewController.h>
#import "MPPVideoWorkout.h"
#import "MPPWorkout.h"

@protocol MPPVideoPlayDelegate <NSObject>

- (void)playTrack:(NSInteger)trackNumber;
- (void)finishedWorkout;
@end

@interface MPPClassVideoContainerViewController : ECSlidingViewController
- (instancetype)initWithWorkout:(MPPWorkout *)workout andVideoWorkout:(MPPVideoWorkout *)videoWorkout;
@end
