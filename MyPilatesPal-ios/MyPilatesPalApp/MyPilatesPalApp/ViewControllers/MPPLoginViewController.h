//
//  MPPLoginViewController.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 1/4/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPPLoginViewController : UIViewController
+ (UINavigationController *)controller;
@end
