//
//  MPPClassFinishedViewController.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 12/29/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface MPPClassFinishedViewController : GAITrackedViewController

- (id)initWithFinishedClassName:(NSString *)finishedClassName;

@end
