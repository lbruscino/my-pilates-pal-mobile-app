//
//  MPPPose.m
//  MyPilatesPal
//
//  Created by PAWAN POUDEL on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import "MPPPose.h"

@implementation MPPPose
+ (instancetype)poseFromDictionary:(NSDictionary *)dictionary {
    MPPPose *pose = [[MPPPose alloc] init];
    NSDictionary *poseDictionary = [dictionary valueForKey:@"ordered_pose"];
    pose.name = [[poseDictionary valueForKey:@"pose"] valueForKey:@"name"];
    pose.duration = [[poseDictionary valueForKey:@"pose"] valueForKey:@"duration"];
    pose.imagePath = [[poseDictionary valueForKey:@"pose"] valueForKey:@"image_url"];
    pose.repetition = [[poseDictionary valueForKey:@"pose"] valueForKey:@"repetition"];
    pose.soundTrackPath = [[poseDictionary valueForKey:@"pose"] valueForKey:@"sound_track_path"];
    pose.uid = [poseDictionary valueForKey:@"pose_id"];
    return pose;
}
@end
