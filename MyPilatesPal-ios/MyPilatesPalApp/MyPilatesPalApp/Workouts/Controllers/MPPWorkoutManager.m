//
//  MPPWorkoutManager.m
//  MyPilatesPal
//
//  Created by PAWAN POUDEL on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import "MPPWorkoutManager.h"
#import "NSMutableURLRequest+CurlDescription.h"
#import "MPPOperationManagerProvider.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking-RACExtensions/AFHTTPRequestOperationManager+RACSupport.h>

NSString *MPPWorkoutManagerError = @"MPPWorkoutManagerError";

@interface MPPWorkoutManager ()
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;
@property (nonatomic, strong) AFHTTPRequestOperationManager *operationManager;
@end

@implementation MPPWorkoutManager

- (id)init {
    self = [super init];
    if(self) {
        self.operationManager = [MPPOperationManagerProvider operationManager];
    }
    return self;
}


#pragma mark - Network activity indicator

- (void)showNetworkActivityIndicator {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)hideNetworkActivityIndicator {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - Fetch random workout

- (void)fetchWorkoutWithCategory:(NSInteger)categoryID
                    withDuration:(NSInteger)duration
                         success:(void (^)(AFHTTPRequestOperation *, id))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackServiceCallWithAction:kServiceActionFetchWorkout
                                  label:[NSString stringWithFormat:@"Call for %li %li", (long)categoryID, (long)duration]
                                  value:nil];
    
    
    NSDictionary *dict = @{ @"category": [NSNumber numberWithInteger:categoryID],
                            @"duration": [NSNumber numberWithInteger:duration] };
    
    
    [self performSelectorOnMainThread:@selector(showNetworkActivityIndicator)
                           withObject:nil
                        waitUntilDone:NO];
    
    [self.operationManager POST:@"api/v1/studios/random"
                     parameters:dict
                        success:success
                        failure:failure];
}


- (void)fetchVideoWorkout:(NSInteger)workoutID
                  success:(void (^)(AFHTTPRequestOperation *, id))success
                  failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackServiceCallWithAction:kServiceActionFetchWorkoutVideo
                                  label:[NSString stringWithFormat:@"Call for %li", (long)workoutID]
                                  value:nil];
    
    
    [self performSelectorOnMainThread:@selector(showNetworkActivityIndicator) withObject:nil waitUntilDone:NO];
    
    [self.operationManager GET:[NSString stringWithFormat:@"api/v2/workouts/%li/videos?ext=.mp4&preset=web&type=videos", (long)workoutID]
                    parameters:nil
                       success:success
                       failure:failure];
}

@end
