//
//  MPPAnalyticsTracker.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 12/14/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

@import Foundation;

static NSString *kEventUiAction = @"UI";
static NSString *kEventServiceAction = @"Service";
static NSString *kEventErrorAction = @"Error";
static NSString *kActionButton = @"Button";
static NSString *kActionSystem = @"System";
static NSString *kActionSwipe = @"Swipe";

static NSString *kApplicationPushRegistration = @"Push Registration";
static NSString *kServiceActioniRate = @"iRate";
static NSString *kServiceActionReceivedAsset = @"Received Asset";
static NSString *kServiceActionInvalidAsset = @"Invalid Asset";
static NSString *kServiceActionFetchWorkout = @"Fetch Workout";
static NSString *kServiceActionFetchWorkoutFailed = @"Fetch Workout Failed";
static NSString *kServiceActionFetchWorkoutCategories = @"Fetch Workout Categories";
static NSString *kServiceActionFetchWorkoutVideo = @"Fetch Workout Video";
static NSString *kServiceActionFetchWorkoutVideoFailed = @"Fetch Workout Video Failed";


@interface MPPAnalyticsTracker : NSObject
- (void)trackUserInteractionWithAction:(NSString *)action label:(NSString *)label value:(NSNumber *)value;
- (void)trackServiceCallWithAction:(NSString *)action label:(NSString *)label value:(NSNumber *)value;
- (void)trackErrorAction:(NSString *)action label:(NSString *)label value:(NSNumber *)value;
- (void)trackAppAction:(NSString *)action label:(NSString *)label value:(NSNumber *)value;
@end
