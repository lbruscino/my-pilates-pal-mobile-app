//
//  MPPWorkout.h
//  MyPilatesPal
//
//  Created by PAWAN POUDEL on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

@interface MPPWorkout : NSObject

+ (instancetype)workoutFromDictionary:(NSDictionary *)dictionary;
@property (nonatomic) NSNumber *uid;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *instructor;
@property (nonatomic, strong) NSString *duration;
@property (nonatomic, strong) NSArray *poses;

@end
