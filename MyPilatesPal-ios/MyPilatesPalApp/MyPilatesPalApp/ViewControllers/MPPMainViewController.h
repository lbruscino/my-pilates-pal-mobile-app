//
//  MPPMainViewController.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPPBaseViewController.h"

@interface MPPMainViewController : MPPBaseViewController

@end
