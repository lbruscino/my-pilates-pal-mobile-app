//
//  MPPBaseViewController.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 11/26/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface MPPBaseViewController : GAITrackedViewController
@property (nonatomic, assign) UIInterfaceOrientation currentInterfaceOrientation;

- (BOOL)shouldShowBackButton;
@end
