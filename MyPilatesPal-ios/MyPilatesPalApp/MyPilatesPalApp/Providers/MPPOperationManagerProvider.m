//
//  MPPOperationManagerProvider.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 1/4/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "MPPOperationManagerProvider.h"

@implementation MPPOperationManagerProvider
+ (AFHTTPRequestOperationManager*)operationManager {
#if DEBUG
    NSURL *baseURL = [NSURL URLWithString:@"https://alop.herokuapp.com/"];
#else
    NSURL *baseURL = [NSURL URLWithString:@"https://mypilatespal.herokuapp.com/"];
#endif
    
    AFHTTPRequestOperationManager *sessionManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [sessionManager.requestSerializer setValue:@"MPP-Allow-API-Call" forHTTPHeaderField:@"X-3scale-Proxy-Secret-Token"];
    sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    return sessionManager;
}
@end
