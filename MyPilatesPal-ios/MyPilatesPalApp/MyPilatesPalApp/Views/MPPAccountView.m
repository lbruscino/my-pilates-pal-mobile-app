//
//  MPPAccountView.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 1/3/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import "MPPAccountView.h"
#import "MPPActionsTableViewController.h"
#import "MPPLoginViewController.h"
#import "MPPRegisterViewController.h"

@interface MPPAccountView ()<UIPopoverControllerDelegate, MPPActionsDelegate>
@property (nonatomic) UITapGestureRecognizer *tappedRecognizer;
@property (nonatomic) NSArray *loggedInActions;
@property (nonatomic) NSArray *loggedOutActions;
@property (nonatomic) UIPopoverController *popover;
- (void)tapped;
@end

@implementation MPPAccountView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    self.tappedRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped)];
    self.tappedRecognizer.numberOfTapsRequired = 1;
    [self addGestureRecognizer:self.tappedRecognizer];
    self.loggedOutActions = @[@"Register", @"Sign In"];
    self.loggedInActions = @[@"Account", @"Sign Out"];
    self.layer.cornerRadius = 4;
    self.layer.borderColor = [UIColorFromRGB(0xE59B11) CGColor];
    self.layer.borderWidth = 4;
    return self;
    
}

- (void)tapped {
    UIColor *popoverColor = UIColorFromRGB(0xE3ECD7);
    MPPActionsTableViewController *actionController = [[MPPActionsTableViewController alloc] initWithActions:self.loggedOutActions withActions:self];
    actionController.tableColor = popoverColor;
    self.popover = [[UIPopoverController alloc]initWithContentViewController:actionController];
    self.popover.backgroundColor = popoverColor;
    [self.popover presentPopoverFromRect:self.frame inView:self.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    [self.popover setDelegate:self];
    self.popover.popoverContentSize = CGSizeMake(200, actionController.tableView.contentSize.height);
}

#pragma mark MPPActionsDelegate methods
- (void)action:(NSString *)action {
    [self.popover dismissPopoverAnimated:YES];
    if([action isEqualToString:@"Register"]) {
        UIViewController *controller = [MPPRegisterViewController controller];
        [self.navigationController presentViewController:controller animated:YES completion:nil];
    } else if([action isEqualToString:@"Sign In"]) {
        UIViewController *controller = [MPPLoginViewController controller];
        [self.navigationController presentViewController:controller animated:YES completion:nil];
    } else if([action isEqualToString:@"Account"]) {
        
    } else {//Sign out
        
    }
}

#pragma mark UIPopoverControllerDelegate methods
- (void)popoverController:(UIPopoverController *)popoverController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView **)view {
    [popoverController dismissPopoverAnimated:YES];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    self.popover = nil;
}

@end
