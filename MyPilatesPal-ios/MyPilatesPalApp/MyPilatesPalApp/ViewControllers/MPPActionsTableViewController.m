//
//  MPPActionsTableViewController.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 1/3/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import "MPPActionsTableViewController.h"

@interface MPPActionsTableViewController ()
@property (nonatomic) NSArray *tableActions;
@property (weak, nonatomic) id<MPPActionsDelegate> tableActionsDelegate;
@end

@implementation MPPActionsTableViewController

- (instancetype)initWithActions:(NSArray *)actions withActions:(id<MPPActionsDelegate>)actionsDelegate {
    self = [super init];
    self.tableActions = actions;
    self.tableActionsDelegate = actionsDelegate;
    return self;
}

- (void)viewDidLoad {
    [self.tableView reloadData];
}

#pragma mark UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *action = [self.tableActions objectAtIndex:indexPath.row];
    if(self.tableActionsDelegate != nil) {
        [self.tableActionsDelegate action:action];
    }
}

#pragma mark UITableViewDataSource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableActions.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    NSString *action = [self.tableActions objectAtIndex:indexPath.row];
    cell.textLabel.text = action;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = self.tableColor;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

@end
