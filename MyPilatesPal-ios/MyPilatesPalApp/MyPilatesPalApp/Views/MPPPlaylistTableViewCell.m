//
//  MPPPlaylistTableViewCell.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 4/7/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import "MPPPlayListTableViewCell.h"

@interface MPPPlayListTableViewCell ()

@end

@implementation MPPPlayListTableViewCell

- (void)awakeFromNib {
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

@end
