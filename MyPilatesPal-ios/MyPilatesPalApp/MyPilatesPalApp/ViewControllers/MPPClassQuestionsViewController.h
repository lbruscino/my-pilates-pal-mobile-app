//
//  MPPClassQuestionsViewController.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 11/12/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPPBaseViewController.h"

@interface MPPClassQuestionsViewController : MPPBaseViewController

- (id)initWithWorkoutType:(MPPWorkoutType)workoutType;

@end
