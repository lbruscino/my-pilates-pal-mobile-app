//
//  NSNumber+Duration.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 4/7/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Duration)
- (NSString *)durationInterval;
@end
