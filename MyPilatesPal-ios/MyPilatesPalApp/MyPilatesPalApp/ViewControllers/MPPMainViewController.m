//
//  MPPMainViewController.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import "MPPMainViewController.h"
#import "MPPClassesViewController.h"
#import "MBProgressHUD.h"
#import "MPPClassQuestionsViewController.h"
#import "MPPClassFinishedViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "MPPAppDelegate.h"
#import "MPPAccountView.h"

@interface MPPMainViewController ()
- (IBAction)stretching:(id)sender;
- (IBAction)pilates:(id)sender;
- (IBAction)poweringPilates:(id)sender;
@property (assign, nonatomic) BOOL didFinishClass;
@property (strong, nonatomic) NSString *finishedClass;
@property (strong, nonatomic) IBOutlet UISegmentedControl *workoutTypeSetting;
@property (weak, nonatomic) IBOutlet MPPAccountView *accountView;
@end

@implementation MPPMainViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.accountView.navigationController = self.navigationController;    
    MPPWorkoutTypeSetting workoutTypeSetting = [[MPPAppDelegate appDelegate] workoutTypeSetting];
    if(workoutTypeSetting == MPPWorkoutTypeSettingFlashCard) {
        self.workoutTypeSetting.selectedSegmentIndex = 0;
    } else {
        self.workoutTypeSetting.selectedSegmentIndex = 1;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setScreenName:@"Main"];
}

#pragma mark IBAction actions
- (void)getWorkoutType:(MPPWorkoutType)workoutType {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    ;
    [tracker trackUserInteractionWithAction:kActionButton
                                       label:[NSString stringWithFormat:@"Workout Type %@", @(workoutType)]
                                       value:nil];
    MPPClassQuestionsViewController *questionsViewController = [[MPPClassQuestionsViewController alloc] initWithWorkoutType:workoutType];
    [self.navigationController pushViewController:questionsViewController animated:YES];
}

- (IBAction)changeWorkoutType:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    if(segmentedControl.selectedSegmentIndex == 0) {
        [[MPPAppDelegate appDelegate] setWorkoutTypeSetting:MPPWorkoutTypeSettingFlashCard];
    } else {
        [[MPPAppDelegate appDelegate] setWorkoutTypeSetting:MPPWorkoutTypeSettingVideo];
    }
}


- (IBAction)stretching:(id)sender {
    [self getWorkoutType:MPPWorkoutTypeStretching];

}

- (IBAction)pilates:(id)sender {
    [self getWorkoutType:MPPWorkoutTypePilates];
}

- (IBAction)poweringPilates:(id)sender {
    [self getWorkoutType:MPPWorkoutTypePowerPilates];
}

#pragma mark private methods
- (void)classFinished:(NSNotification *)notification {
    DebugLog(@"Class finished");
    self.didFinishClass = YES;
    self.finishedClass = [notification.userInfo objectForKey:@"className"];
}

- (BOOL)shouldShowBackButton {
    return NO;
}

@end
