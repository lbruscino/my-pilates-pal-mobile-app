//
//  MPPWorkout.m
//  MyPilatesPal
//
//  Created by PAWAN POUDEL on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import "MPPWorkout.h"
#import "MPPPose.h"

@implementation MPPWorkout

+ (instancetype)workoutFromDictionary:(NSDictionary *)dictionary {
    MPPWorkout *result = [[MPPWorkout alloc] init];
    result.uid = [dictionary valueForKey:@"id"];
    result.title = [dictionary valueForKey:@"title"];
    result.instructor = [[dictionary valueForKey:@"instructor"] valueForKey:@"name"];
    result.duration = [[dictionary valueForKey:@"description"] valueForKey:@"description"];
    
    NSArray *poseDictionaries = [dictionary valueForKey:@"ordered_poses"];
    NSMutableArray *mutablePoses = [NSMutableArray new];
    for (NSDictionary *poseDictionary in poseDictionaries) {
        [mutablePoses addObject:[MPPPose poseFromDictionary:poseDictionary]];
    }
    result.poses = mutablePoses;
    
    return result;
}

@end