//
//  MPPVideoPlayListTableViewController.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 4/4/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPPVideoWorkout.h"
#import "MPPClassVideoContainerViewController.h"
@interface MPPVideoPlayListTableViewController : UITableViewController
- (instancetype)initWithVideoWorkout:(MPPVideoWorkout *)workout;
@property (nonatomic, assign) id<MPPVideoPlayDelegate> videoPlayDelegate;
@end
