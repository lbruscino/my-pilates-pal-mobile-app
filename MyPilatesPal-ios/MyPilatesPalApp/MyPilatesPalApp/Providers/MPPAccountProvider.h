//
//  MPPAccountProvider.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 1/4/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPPAccountProvider : NSObject
+ (instancetype)provider;

- (void)loginWith:(NSString *)login
     withPassword:(NSString *)password
          success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)registerWith:(NSString *)login
        withPassword:(NSString *)password
             andName:(NSString *)name
             success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
@end
