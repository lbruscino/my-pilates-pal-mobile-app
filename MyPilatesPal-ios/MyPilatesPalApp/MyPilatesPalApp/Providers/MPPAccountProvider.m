//
//  MPPAccountProvider.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 1/4/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import "MPPAccountProvider.h"
#import <AFNetworking/AFNetworking.h>
#import "MPPOperationManagerProvider.h"

@interface MPPAccountProvider ()
@property (nonatomic) AFHTTPRequestOperationManager *manager;
@end

@implementation MPPAccountProvider
+ (instancetype)provider {
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}

- (instancetype)init {
    self = [super init];
    self.manager = [MPPOperationManagerProvider operationManager];
    return self;
}

- (void)loginWith:(NSString *)login
     withPassword:(NSString *)password
          success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    NSDictionary *dict = @{ @"user": @{ @"email": login, @"password": password }};
    [self.manager POST:@"api/v1/studios/random"
            parameters:dict
               success:success
               failure:failure];
}

- (void)registerWith:(NSString *)login
        withPassword:(NSString *)password
             andName:(NSString *)name
             success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    NSDictionary *dict = @{ @"user": @{ @"email": login, @"password": password }};
    [self.manager POST:@"api/v1/register"
            parameters:dict
               success:success
               failure:failure];
    
}
@end
