//
//  MPPClassFinishedViewController.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 12/29/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import "MPPClassFinishedViewController.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import <iRate/iRate.h>

@interface MPPClassFinishedViewController ()<MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *className;
@property (strong, nonatomic) NSString *finishedClass;
@end

@implementation MPPClassFinishedViewController

- (id)initWithFinishedClassName:(NSString *)finishedClassName {
    self = [self init];
    if(self) {
        self.finishedClass = finishedClassName;
    }
    return self;
}

- (void)viewDidLoad {
    [self.className setText:self.finishedClass];
    [self setScreenName:@"Class Finished"];
    
    [[iRate sharedInstance] logEvent:NO];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

#pragma mark private methods
- (IBAction)shareFacebook:(id)sender {
    [self shareForServiceType:SLServiceTypeFacebook];
}
- (IBAction)shareTwitter:(id)sender {
    [self shareForServiceType:SLServiceTypeTwitter];
}


- (IBAction)shareEmail:(id)sender {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionButton label:@"Email" value:nil];
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    [picker setMailComposeDelegate:self];
    [picker setSubject:[NSString stringWithFormat:@"Just took the %@ from A Lot of Pilates.", self.finishedClass]];
    [picker setMessageBody:@"<p>I just finished a customized Pilates class from <a href='http://www.alotofpilates.com'>ALotOfPilates.com</a></p><p>I love it! You get a different Pilates sequence each time so you don't get stuck in a boring routine. Best of all, it's free! <a href='http://www.alotofpilates.com'>Become my pal and we can workout together!</a></p>" isHTML:YES];
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)shareForServiceType:(NSString *)serviceType {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    if([SLComposeViewController isAvailableForServiceType:serviceType]) {
        [tracker trackUserInteractionWithAction:kActionButton
                                           label:serviceType
                                           value:nil];
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:serviceType];
        SLComposeViewControllerCompletionHandler __block completionHandler = ^(SLComposeViewControllerResult result) {
            [controller dismissViewControllerAnimated:YES completion:nil];
        };
        [controller addURL:[NSURL URLWithString:@"http://www.alotofpilates.com"]];
        [controller setInitialText:[NSString stringWithFormat:[self messageFor:serviceType], self.finishedClass]];
        [controller addImage:[UIImage imageNamed:@"AppIcon"]];
        [controller setCompletionHandler:completionHandler];
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        [tracker trackUserInteractionWithAction:kActionButton
                                           label:[NSString stringWithFormat:@"Unable to share %@", serviceType]
                                           value:nil];
        [self showAlert:serviceType];
    }
}

- (NSString *)messageFor:(NSString *)serviceType {
    if([serviceType isEqualToString:SLServiceTypeTwitter]) {
        return @"Just took the %@ workout @ALotOfPilates at alotofpilates.com";
    } else {
        return @"Just took the %@ workout @ ALotOfPilates.com";
    }
}

- (void)showAlert:(NSString *)serviceType {
    NSString *message;
    if([serviceType isEqualToString:SLServiceTypeFacebook]) {
        message = @"You will need to connect your Facebook account in order to use this feature. This is located under Settings -> Facebook";
    } else {
        message = @"You will need to connect your Twitter account in order to use this feature. This is located under Settings -> Twitter";
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Unable to share"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
    [alertView show];
}


#pragma mark MFMailComposeViewControllerDelegate methods
- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)takeAnotherClass:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
