//
//  NSNumber+Duration.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 4/7/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import "NSNumber+Duration.h"

@implementation NSNumber (Duration)
- (NSString *)durationInterval {
    NSInteger ti = [self integerValue];
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}
@end
