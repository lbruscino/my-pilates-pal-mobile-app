//
//  UIViewController+AdBanner.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 4/11/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import "UIViewController+AdBanner.h"
#import <Masonry/Masonry.h>

@implementation UIViewController (AdBanner)
- (void)addBannerWithDelegate:(id<ADBannerViewDelegate>)bannerDelegate {
    ADBannerView *adView = [[ADBannerView alloc] init];
    adView.delegate = bannerDelegate;
    [adView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [self.view addSubview:adView];
    
    [adView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.bottom.equalTo(self.view.mas_bottom);
        make.right.equalTo(self.view.mas_right);
    }];
}
@end
