//
//  NSString+AssetType.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 3/9/14.
//  Copyright (c) 2014 My Pilates Pal. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, NSAssetType) {
    NSAssetTypeAudio,
    NSAssetTypeImage
};
@interface NSString (AssetType)
- (NSAssetType)assetType;
@end
