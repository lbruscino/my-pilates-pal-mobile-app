//
//  MPPVideoWorkout.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 8/5/14.
//  Copyright (c) 2014 My Pilates Pal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPPVideoWorkout : NSObject
+ (instancetype)videoWorkoutFromDictionary:(NSDictionary *)dictionary;
@property (nonatomic, strong) NSArray *videos;
@end
