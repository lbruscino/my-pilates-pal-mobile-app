//
//  MPPClassVideoViewController.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 11/15/14.
//  Copyright (c) 2014 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPPVideoWorkout.h"
#import "MPPWorkout.h"
#import "MPPVideo.h"
#import "MPPClassVideoContainerViewController.h"

@interface MPPClassVideoViewController : UIViewController
@property (strong, nonatomic) MPPVideoWorkout *videoWorkout;
@property (assign, nonatomic) id<MPPVideoPlayDelegate> videoPlayDelegate;
- (void)playStreamFromBeginning;
- (void)playStreamAtIndex:(NSInteger)streamIndex;
- (void)pauseCurrent;
- (void)resumeCurrent;
@end
