//
//  MPPPlaylistTableViewCell.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 4/7/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPPPlayListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *videoAsset;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UILabel *videoLength;
@end
