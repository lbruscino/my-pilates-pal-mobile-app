//
//  MPPOperationManagerProvider.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 1/4/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPPOperationManagerProvider : NSObject
+ (AFHTTPRequestOperationManager*)operationManager;
@end
