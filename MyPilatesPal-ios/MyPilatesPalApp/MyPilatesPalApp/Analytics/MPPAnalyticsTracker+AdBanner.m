//
//  MPPAnalyticsTracker+AdBanner.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 12/8/15.
//  Copyright © 2015 My Pilates Pal. All rights reserved.
//

#import "MPPAnalyticsTracker+AdBanner.h"

static NSString *kBannerActionBegan = @"Banner Action Began";
static NSString *kBannerActionFinished = @"Banner Action Finished";
static NSString *kBannerWillLoad = @"Banner Will Load";
static NSString *kBannerDidLoad = @"Banner Did Load";
static NSString *kBannerDidFail = @"Banner Did Fail";

@implementation MPPAnalyticsTracker (AdBanner)
- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    [self trackAppAction:kBannerDidFail label:error.description value:nil];
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner {
    [self trackAppAction:kBannerActionFinished label:nil value:nil];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
    [self trackAppAction:kBannerDidLoad label:nil value:nil];
}

- (void)bannerViewWillLoadAd:(ADBannerView *)banner {
    [self trackAppAction:kBannerWillLoad label:nil value:nil];
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave {
    [self trackUserInteractionWithAction:kBannerActionBegan label:nil value:nil];
    return YES;
}
@end
