//
//  MPPWorkoutManagerDelegate.h
//  MyPilatesPal
//
//  Created by PAWAN POUDEL on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import "MPPWorkout.h"
#import "MPPVideoWorkout.h"

@protocol MPPWorkoutManagerDelegate <NSObject>

@optional

- (void)fetchingCategoriesFailedWithError:(NSError *)error;
- (void)didReceiveCategories:(NSArray *)categories;

- (void)fetchingRandomWorkoutFailedWithError:(NSError *)error;
- (void)didReceiveRandomWorkout:(MPPWorkout *)workout;

- (void)fetchRandomVideoWorkoutFailedWithError:(NSError *)error;
- (void)didReceiveRandomVideoWorkout:(NSArray *)videoWorkouts;

@end
