//
//  MPPClassesViewController.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 9/28/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import "MPPClassesViewController.h"
#import "MPPPose.h"
#import "MBProgressHUD.h"
#import "MPPAnalyticsTracker.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "MPPClassFinishedViewController.h"
#import "MPPDownloadManager.h"
#import "NSString+AssetType.h"
#import "UIViewController+AdBanner.h"
@import iAd;

typedef NS_ENUM(NSInteger, MPPClassStatus) {
    MPPClassStatusInitialized,
    MPPClassStatusLoading,
    MPPClassStatusStarted,
    MPPClassStatusPaused,
    MPPClassStatusResumed
};

@interface MPPClassesViewController () <MPPDownloadManagerDelegate, AVAudioPlayerDelegate, UIGestureRecognizerDelegate>
@property (assign, nonatomic) int poseNumber;
@property (strong, nonatomic) NSString *assetPath;
@property (weak, nonatomic) IBOutlet UILabel *className;
@property (weak, nonatomic) IBOutlet UILabel *classLength;
@property (strong, nonatomic) MPPWorkout *classWorkout;
@property (weak, nonatomic) IBOutlet UIButton *controlWorkout;
@property (strong, nonatomic) NSTimer *poseTimer;
@property (strong, nonatomic) MPPPose *firstPose;
@property (strong, nonatomic) NSURLSession *urlSession;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (assign, nonatomic) MPPClassStatus classStatus;
@property (assign, nonatomic) BOOL pausedWorkout;
@property (strong, nonatomic) UISwipeGestureRecognizer *swipeGesture;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (strong, nonatomic) MPPDownloadManager *downloadManager;
@property (weak, nonatomic) IBOutlet UIButton *nextArrow;
@property (nonatomic) NSInteger failedDownloadAttempts;
@property (weak, nonatomic) IBOutlet UIView *poseContainer;
@property (weak, nonatomic) IBOutlet UIImageView *poseImageView;
@property (weak, nonatomic) IBOutlet UILabel *repetitionLabel;
@property (weak, nonatomic) IBOutlet UILabel *exerciseLabel;
//Control workout constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toBottomConstraint;

- (IBAction)startWorkout:(id)sender;
- (IBAction)nextSequence:(id)sender;
@end

@implementation MPPClassesViewController
@synthesize classWorkout;
@synthesize controlWorkout;

- (id)initWithWorkout:(MPPWorkout *)workout {
    self = [self init];
    if (self) {
        classWorkout = workout;
        self.poseNumber = 0;
        self.failedDownloadAttempts = 0;
        self.classStatus = MPPClassStatusInitialized;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self registerForNotifications];
    [self.className setText:classWorkout.title];
    [self.classLength setText:classWorkout.duration];
    self.nextArrow.hidden = YES;
    self.poseImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.poseImageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    
    self.downloadManager = [[MPPDownloadManager alloc] initWithDelegate:self withList:[self createList]];
    
    self.swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                  action:@selector(swipeLeft:)];
    [self.swipeGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.swipeGesture setDelegate:self];
    [self.view addGestureRecognizer:self.swipeGesture];
    
    [self addBannerWithDelegate:[[MPPAnalyticsTracker alloc] init]];
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    BOOL shouldExecuteAction = [self allowActionToRun]; // your app implements this method
    if (!willLeave && shouldExecuteAction)
    {
        [self pauseWorkout];
    }
    return shouldExecuteAction;
}

- (BOOL)allowActionToRun {
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner {
    NSLog(@"Called when a new banner advertisement is loaded.");
     [self resumeWorkout];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self setScreenName:[NSString stringWithFormat:@"Workout %@", self.classWorkout.title]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.urlSession = nil;
    [self.audioPlayer stop];
    [self.poseTimer invalidate];
    self.poseTimer = nil;
    self.audioPlayer = nil;
}

- (void)dealloc {
    self.urlSession = nil;
    [self.audioPlayer stop];
    [self.poseTimer invalidate];
    self.poseTimer = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSArray *)createList {
    __block NSMutableArray *array = [[NSMutableArray alloc] init];

    [self.classWorkout.poses enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        MPPPose *pose = (MPPPose *)obj;
        [array addObject:pose.imagePath];
        [array addObject:pose.soundTrackPath];
    }];
    return array;
}

- (BOOL)isLastPose {
    if(self.poseNumber < [self.classWorkout.poses count]) {
        return NO;
    } else {
        return YES;
    }
}

- (MPPPose *)nextPose {
    if(self.poseNumber < [self.classWorkout.poses count]) {
        MPPPose *result = [self.classWorkout.poses objectAtIndex:self.poseNumber];
        self.poseNumber++;
        return result;
    } else {
        return nil;
    }
}

- (MPPPose *)currentPose {
    NSInteger currentPoseNumber = self.poseNumber - 1;
    if(currentPoseNumber >= 0) {
        return [self.classWorkout.poses objectAtIndex:currentPoseNumber];
    } else {
        return nil;
    }
}

- (IBAction)startWorkout:(id)sender {
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    UIButton *senderButton = (UIButton *)sender;
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionButton label:senderButton.titleLabel.text value:nil];

    if(self.classStatus == MPPClassStatusInitialized) {
        self.classStatus = MPPClassStatusLoading;
        MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
        [tracker trackUserInteractionWithAction:kActionButton label:@"Loading Workout" value:nil];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.verticalConstraint.priority = 750;
            self.toBottomConstraint.priority = 999;
            [self.controlWorkout setAlpha:0.0f];
            [self.view layoutIfNeeded];
        } completion:nil];
        
        [self showHUD];
        [self.downloadManager start];
    } else if(self.classStatus == MPPClassStatusPaused) {
        [self resumeWorkout];
    } else {
        [self pauseWorkout];
    }
}

- (IBAction)nextSequence:(id)sender {
    MPPPose *currentPose = [self currentPose];
    if(currentPose != nil) {
        MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
        [tracker trackUserInteractionWithAction:kActionButton
                                          label:[NSString stringWithFormat:@"Skip Workout %@", currentPose.name]
                                          value:nil];
    }
    [self nextSequence];
}


- (void)registerForNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseFromBackgroundWorkout) name:UIApplicationDidEnterBackgroundNotification object:nil];;
}

- (void)pauseFromBackgroundWorkout {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionSystem label:@"Pause Workout" value:nil];
    [self pauseWorkout];
}

- (void)updateClassControlButton {
    switch (self.classStatus) {
        case MPPClassStatusStarted:
        case MPPClassStatusResumed: {
            [controlWorkout setTitle:@"Pause Workout" forState:UIControlStateNormal];
            [controlWorkout setImage:[UIImage imageNamed:@"Pause"] forState:UIControlStateNormal];
            [controlWorkout setBackgroundColor:UIColorFromRGB(0x4187A9)];
            break;
        }
        case MPPClassStatusPaused: {
            [self.controlWorkout setTitle:@"Resume Workout" forState:UIControlStateNormal];
            [self.controlWorkout setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
            [self.controlWorkout setBackgroundColor:UIColorFromRGB(0x85AC5C)];
            break;
        }
        default:
            break;
    }

}

- (void)resumeWorkout {
    self.classStatus = MPPClassStatusResumed;
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionButton label:@"Resume Workout" value:nil];
    [self updateClassControlButton];
    [self readyNextPose];
}

- (void)pauseWorkout {
    self.classStatus = MPPClassStatusPaused;
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionButton label:@"Pause Workout" value:nil];
    [self updateClassControlButton];
    [self.poseTimer invalidate];
    self.poseTimer = nil;
}

#pragma mark Workout Scrolling methods

- (void)startPoseSequence {
    self.nextArrow.hidden = NO;
    self.classStatus = MPPClassStatusStarted;
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionButton label:@"Started Workout" value:nil];
    
    [self updateClassControlButton];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.controlWorkout layoutIfNeeded];
        [self.controlWorkout setAlpha:1.0f];
    } completion:nil];
    
    [self hideHUD];
    [self readyNextPose];
}

- (void)updatePose:(MPPPose *)pose {
    [self.poseImageView setImage:pose.locallyCachedImage];
    [self.exerciseLabel setText:pose.name];
    [self.repetitionLabel setText:pose.repetition];
}

- (void)readyNextPose {
    if(self.poseNumber < [self.classWorkout.poses count]) {
        MPPPose *pose = [self nextPose];
        [self updatePose:pose];

        NSTimeInterval timeInterval;
#if DEBUG
        timeInterval = 10.0f;
#else
        timeInterval = [[pose duration] floatValue];
#endif
        
        self.poseTimer = [NSTimer scheduledTimerWithTimeInterval:timeInterval
                                                          target:self
                                                        selector:@selector(readyNextPose)
                                                        userInfo:nil
                                                         repeats:NO];
        
//        [UIView animateWithDuration:0.5 animations:^{
//            [self.lastController.view layoutIfNeeded];
//        }];
        
        NSError *err = nil;
        self.audioPlayer = [[AVAudioPlayer alloc] initWithData:pose.locallyCachedSound error:&err];
        self.audioPlayer.delegate = self;
        [self.audioPlayer prepareToPlay];
        [self.audioPlayer play];
        
        DebugLog(@"Pose: %i out of %i", self.poseNumber, self.classWorkout.poses.count);
    } else {
        DebugLog(@"End workout");
        [self endWorkout];
    }
}

- (void)endWorkout {
    MPPClassFinishedViewController *classFinished = [[MPPClassFinishedViewController alloc] initWithFinishedClassName:classWorkout.title];
    UINavigationController * navigationController = self.navigationController;
    [navigationController popToRootViewControllerAnimated:NO];
    [navigationController pushViewController:classFinished animated:YES];
}

- (void)failedToDownload {
    [self hideHUD];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Failed to start workout"
                                                        message:@"Please check your network connection and try again."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
    
    self.failedDownloadAttempts++;
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackUserInteractionWithAction:kActionButton
                                      label:@"Failed Download"
                                      value:@(self.failedDownloadAttempts)];
    self.poseNumber = 0;
    
    self.verticalConstraint.priority = 999;
    self.toBottomConstraint.priority = 750;
    [self.controlWorkout setAlpha:1.0f];
    self.classStatus = MPPClassStatusInitialized;
}


#pragma mark AVAudioPlayerDelegate methods
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {

}

#pragma mark MPPDownloadManager methods

- (void)error:(NSError *)error downloading:(NSString *)url current:(NSInteger)current of:(NSInteger)number {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackServiceCallWithAction:kServiceActionInvalidAsset label:url value:nil];
}

- (void)errorDownloading {
    [self performSelectorOnMainThread:@selector(failedToDownload) withObject:nil waitUntilDone:YES];
}

- (void)finishedDownloadingURL:(NSURL *)url withAssetPath:(NSString *)assetPath current:(NSInteger)current of:(NSInteger)number {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    [tracker trackServiceCallWithAction:kServiceActionReceivedAsset label:assetPath value:nil];
    [self updateHUDWithCurrent:current outOf:number];
    
    int poseIndex = (int)current / 2;
    DebugLog(@"%i index is %i current", poseIndex, current);
    if([assetPath assetType] == NSAssetTypeImage) {
        MPPPose *poseInfo = [self.classWorkout.poses objectAtIndex:poseIndex];
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        UIImage *image = [UIImage imageWithData:imageData];
        [poseInfo setLocallyCachedImage:image];
    } else {
        NSData *soundData = [NSData dataWithContentsOfURL:url];
        MPPPose *poseInfo = [self.classWorkout.poses objectAtIndex:poseIndex];
        [poseInfo setLocallyCachedSound:soundData];
        [poseInfo setLocallySoundURL:url];
    }
}

- (void)finishedDownloading:(NSInteger)successful outOf:(NSInteger)total {
    if(successful == total) {
        [self performSelectorOnMainThread:@selector(startPoseSequence) withObject:nil waitUntilDone:YES];
    } else {
        [self performSelectorOnMainThread:@selector(failedToDownload) withObject:nil waitUntilDone:YES];
    }
}

#pragma mark MBProgressHUD methods
- (void)showHUD {
    self.progressHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:self.progressHUD];
    self.progressHUD.mode = MBProgressHUDModeDeterminate;
    self.progressHUD.labelText = @"Loading workout...";
    [self.progressHUD show:YES];
}

- (void)hideHUD {
    [self.progressHUD hide:YES];
}

- (void)updateHUDWithCurrent:(NSInteger)current outOf:(NSInteger)total {
    [self.progressHUD setProgress:((float)current/(float)total)];
}

#pragma mark UIGestureRecognizer methods

- (void)swipeLeft:(id)sender {
    MPPPose *currentPose = [self currentPose];
    if(currentPose != nil) {
        MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
        [tracker trackUserInteractionWithAction:kActionSwipe
                                          label:[NSString stringWithFormat:@"Skip Workout %@", currentPose.name]
                                          value:nil];
    }
    [self nextSequence];
}

- (void)nextSequence {
    if([self isLastPose]) {
        [self endWorkout];
    } else {
        if(self.classStatus == MPPClassStatusResumed || self.classStatus == MPPClassStatusStarted) {
            [self.poseTimer invalidate];
            self.poseTimer = nil;
            [self readyNextPose];
        } else if(self.classStatus == MPPClassStatusPaused) {
            [self resumeWorkout];
        }
    }
}

#pragma mark UIGestureRecognizerDelegate methods
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}

@end
