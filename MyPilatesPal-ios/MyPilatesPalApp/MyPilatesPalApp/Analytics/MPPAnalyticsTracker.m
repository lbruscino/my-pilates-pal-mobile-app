//
//  MPPAnalyticsTracker.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 12/14/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import "MPPAnalyticsTracker.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

@implementation MPPAnalyticsTracker

- (void)trackAppAction:(NSString *)action label:(NSString *)label value:(NSNumber *)value {
    
}

- (void)trackUserInteractionWithAction:(NSString *)action label:(NSString *)label value:(NSNumber *)value {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:kEventUiAction         // Event category (required)
                                                          action:action                 // Event action (required)
                                                           label:label                  // Event label
                                                           value:value] build]];
    
}

- (void)trackServiceCallWithAction:(NSString *)action label:(NSString *)label value:(NSNumber *)value {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:kEventServiceAction    // Event category (required)
                                                          action:action                 // Event action (required)
                                                           label:label                  // Event label
                                                           value:value] build]];
}

- (void)trackErrorAction:(NSString *)action label:(NSString *)label value:(NSNumber *)value {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:kEventErrorAction    // Event category (required)
                                                          action:action                 // Event action (required)
                                                           label:label                  // Event label
                                                           value:value] build]];
}
@end
