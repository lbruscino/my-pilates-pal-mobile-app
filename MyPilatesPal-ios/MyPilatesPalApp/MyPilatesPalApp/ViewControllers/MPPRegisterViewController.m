//
//  MPPRegisterViewController.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 1/4/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import "MPPRegisterViewController.h"
#import "MPPAccountProvider.h"

@interface MPPRegisterViewController ()
@property (weak, nonatomic) IBOutlet UITextField *loginName;
@property (weak, nonatomic) IBOutlet UITextField *loginUsername;
@property (weak, nonatomic) IBOutlet UITextField *loginPassword;
@property (weak, nonatomic) IBOutlet UITextField *loginVerifyPassword;

@end

@implementation MPPRegisterViewController

+ (UINavigationController *)controller {
    UIViewController *controller = [[MPPRegisterViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    return navigationController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)registerUser:(id)sender {
    MPPAccountProvider *provider = [MPPAccountProvider provider];
    
    [provider registerWith:self.loginUsername.text
              withPassword:self.loginPassword.text
                   andName:self.loginName.text
                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                       NSLog(@"Success: %@", responseObject);
                       [self dismissViewControllerAnimated:YES completion:nil];
    }
                   failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

@end
