//
//  MPPVideoPlayListTableViewController.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 4/4/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import "MPPVideoPlayListTableViewController.h"
#import "MPPPlayListTableViewCell.h"
#import "NSNumber+Duration.h"
#import "MPPVideo.h"

@interface MPPVideoPlayListTableViewController ()
@property(nonatomic, strong) MPPVideoWorkout *videoWorkout;
@property(nonatomic, assign) NSInteger current;
@end

@implementation MPPVideoPlayListTableViewController
static NSString *cellIdentifier = @"Cell";
- (instancetype)initWithVideoWorkout:(MPPVideoWorkout *)workout {
    self = [super init];
    self.videoWorkout = workout;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"MPPPlayListTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellIdentifier];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.videoWorkout.videos.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MPPPlayListTableViewCell *cell = (MPPPlayListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    MPPVideo *video = [self.videoWorkout.videos objectAtIndex:indexPath.row];
    cell.videoTitle.text = video.name;
    cell.videoLength.text = [video.duration durationInterval];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MPPAnalyticsTracker *tracker = [[MPPAnalyticsTracker alloc] init];
    MPPVideo *video = [self.videoWorkout.videos objectAtIndex:indexPath.row];
    NSString *userAction = [NSString stringWithFormat:@"Select Video %@", video.name];
    [tracker trackUserInteractionWithAction:@"Select Video" label:userAction value:nil];
    [self.videoPlayDelegate playTrack:indexPath.row];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88;
}

@end
