//
//  MPPBaseViewController.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 11/26/13.
//  Copyright (c) 2013 My Pilates Pal. All rights reserved.
//

#import "MPPBaseViewController.h"
#import <Masonry/Masonry.h>


@implementation MPPBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:[self nibName] bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (NSString *)nibName {
    if(UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        return [NSString stringWithFormat:@"%@-Landscape", NSStringFromClass([self class])];
    } else {
        return [NSString stringWithFormat:@"%@", NSStringFromClass([self class])];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([self shouldShowBackButton]) {
        UIButton *backButton = [[UIButton alloc] init];
        [backButton setImage:[UIImage imageNamed:@"Back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(pop) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:backButton];
        [backButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).with.offset(20);
            make.left.equalTo(self.view).with.offset(15);
            make.height.equalTo(@40);
            make.width.equalTo(@40);
        }];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.currentInterfaceOrientation != [[UIApplication sharedApplication] statusBarOrientation]) {
        [self correctOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    }
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    [self correctOrientation:toInterfaceOrientation];
}

- (void)correctOrientation:(UIInterfaceOrientation)orientation {
    self.currentInterfaceOrientation = orientation;
    if( UIInterfaceOrientationIsLandscape(orientation) )
    {
        [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@-Landscape", NSStringFromClass([self class])]
                                      owner: self
                                    options: nil];
        [self viewDidLoad];
    }
    else
    {
        [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@", NSStringFromClass([self class])]
                                      owner: self
                                    options: nil];
        [self viewDidLoad];
    }
}

- (BOOL)shouldShowBackButton {
    return NO;
}

- (void)pop {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
