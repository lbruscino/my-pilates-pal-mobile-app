//
//  UIViewController+AdBanner.h
//  MyPilatesPalApp
//
//  Created by Evan Anger on 4/11/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import <UIKit/UIKit.h>
@import iAd;

@interface UIViewController (AdBanner)
- (void)addBannerWithDelegate:(id<ADBannerViewDelegate>)bannerDelegate;
@end
