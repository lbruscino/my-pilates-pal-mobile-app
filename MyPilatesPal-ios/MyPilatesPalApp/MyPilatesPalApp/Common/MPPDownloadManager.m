//
//  MPPDownloadManager.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 3/9/14.
//  Copyright (c) 2014 My Pilates Pal. All rights reserved.
//

#import "MPPDownloadManager.h"

@interface MPPDownloadManager() <NSURLSessionDelegate, NSURLSessionTaskDelegate>
@property (nonatomic, strong) NSURLSession *urlSession;
@property (nonatomic, assign) id<MPPDownloadManagerDelegate> sessionDelegate;
@property (nonatomic, strong) NSArray *list;
@property (nonatomic, assign) int current;
@property (nonatomic, strong) NSString *currentAsset;
@property (nonatomic) NSInteger successfullyDownloaded;
@property (nonatomic) BOOL stop;
@end

@implementation MPPDownloadManager

- (id)initWithDelegate:(id<MPPDownloadManagerDelegate>)downloadDelegate withList:(NSArray *)downloadList {
    self = [super init];
    if(self) {
        self.urlSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                    delegate:self
                                               delegateQueue:nil];
        self.sessionDelegate = downloadDelegate;
        self.current = 0;
        self.list = downloadList;
    }
    return self;
}

- (void)start {
    self.successfullyDownloaded = 0;
    self.current = 0;
    [self downloadNext];
}


- (void)downloadNext {
    self.currentAsset = [self.list objectAtIndex:self.current];
    NSURLSessionDownloadTask *downloadTask = [self.urlSession downloadTaskWithURL:[NSURL URLWithString:self.currentAsset]
                                                                completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                                    
                                                                }];
    [downloadTask resume];
}

#pragma mark NSURLSessionDelegate methods

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
    
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error {
    if (error) {
        [self.sessionDelegate error:error downloading:self.currentAsset current:self.current of:self.list.count];
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    NSLog(@"Did Complete With Error");

    if(error) {
        [self.sessionDelegate errorDownloading];
    } else {
        self.successfullyDownloaded++;
        self.current++;
        if(self.current < [self.list count]) {
            [self downloadNext];
        } else {
            [self.sessionDelegate finishedDownloading:self.successfullyDownloaded outOf:self.list.count];
        }
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    [self.sessionDelegate finishedDownloadingURL:location withAssetPath:self.currentAsset
                                  current:self.current
                                       of:self.list.count];
}
@end
