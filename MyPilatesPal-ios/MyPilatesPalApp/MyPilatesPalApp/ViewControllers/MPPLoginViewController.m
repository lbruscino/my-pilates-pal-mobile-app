//
//  MPPLoginViewController.m
//  MyPilatesPalApp
//
//  Created by Evan Anger on 1/4/15.
//  Copyright (c) 2015 My Pilates Pal. All rights reserved.
//

#import "MPPLoginViewController.h"
#import "MPPAccountProvider.h"
#import <AFNetworking/AFNetworking.h>

@interface MPPLoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *loginUser;
@property (weak, nonatomic) IBOutlet UITextField *loginPassword;
@end

@implementation MPPLoginViewController

+ (UINavigationController *)controller {
    UIViewController *controller = [[MPPLoginViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    return navigationController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)login:(id)sender {
    [[MPPAccountProvider provider] loginWith:self.loginUser.text
                                withPassword:self.loginPassword.text
                                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                         [self dismissViewControllerAnimated:YES completion:nil];
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Unable to login" message:@"Please check your credentials" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                         [alertView show];
    }];
}

@end
